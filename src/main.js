import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { globalCookiesConfig } from 'vue3-cookies'
import axios from 'axios'
import VueAxios from 'vue-axios'
import CoreuiVue from '@coreui/vue'
import CIcon from '@coreui/icons-vue'
import { iconsSet as icons } from '@/assets/icons'
import DocsExample from '@/components/DocsExample'

import VueCookies from 'vue-cookies'
import Cookies from 'universal-cookie'
import { API_URL, API_PROJECT, API_VERSION } from '@/config'
axios.defaults.baseURL = `${API_URL}/${API_PROJECT}/${API_VERSION}`
const cookies = new Cookies()
axios.defaults.headers.common['Authorization'] = `Bearer ${cookies.get(
  'token',
)}`
const app = createApp(App)
app.use(VueAxios, axios)
app.use(store)
app.use(VueCookies)
app.use(router)
globalCookiesConfig({
  expireTimes: '1d',
  path: '/',
  domain: '',
  secure: true,
  sameSite: 'None',
})
app.use(CoreuiVue)
app.provide('icons', icons)
app.component('CIcon', CIcon)
app.component('DocsExample', DocsExample)

app.mount('#app')
