describe('Form Pendaftaran', () => {
  it('Mengisi formulir dan mengirimkan', () => {
    cy.visit('/pages/login')


    cy.get('input[name="email"]').type('client-cold-chain-admin@universal-iot.com')
    cy.get('input[name="password"]').type('secret')
    cy.get('button[id="submit"]').click()


    cy.url().should('include', '/dashboard')
  })
})
