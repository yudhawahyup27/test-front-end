describe('Test Tambah Data', () => {
it('Tambah Data Perusahaan', () => {
cy.visit('/create-data')

// Skenario Menambah data
cy.get('input[name="prefix"]').type('test')
cy.get('input[name="name"]').type('PT Test')
cy.get('input[name="address"]').type('Karawang')
    cy.fixture('test.png').then((fileContent) => {
      // Menunggu elemen input file muncul dan memastikan bisa diinteraksi
      cy.get('input[type="file"]').should('exist').as('fileInput');

      // Memasukkan konten file ke dalam elemen input file
      cy.get('@fileInput').attachFile({
        fileContent: fileContent,
        fileName: 'test.png',
        mimeType: 'image/png',
      });

      // Menunggu gambar di-upload dan memeriksa sesuatu
      cy.get('.logo').should('be.visible');
    });
        cy.get('button[id="submit"]').click()
});

});
