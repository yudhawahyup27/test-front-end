describe('Upload Image', () => {
  it('should upload an image', () => {

    cy.visit('/editprofile');


    cy.fixture('test.png').then((fileContent) => {

      cy.get('input[type="file"]').should('exist').as('fileInput');

      cy.get('@fileInput').attachFile({
        fileContent: fileContent,
        fileName: 'test.png',
        mimeType: 'image/png',
      });


      cy.get('.uploaded-image').should('be.visible');
    });
  });
});
